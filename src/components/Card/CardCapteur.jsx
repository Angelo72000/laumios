import React, { Component } from 'react';
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Icon from "@material-ui/core/Icon";
import CustonDropdown from "components/CustonDropdown/CustonDropdown.jsx";

// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import cardStyle from "assets/jss/material-dashboard-react/components/cardStyle.jsx";

function CardCapteur({ ...props }) {
    const {
        classes,
        className,
        cardCategory,
        cardTitle,
        cardData,
        ...rest
    } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [className]: className !== undefined
    });
    return (
        <div className={cardClasses} {...rest}>
            <CardHeader color="success" stats icon>
                <CardIcon color="success">
                    <Icon>compass_calibration</Icon>
                </CardIcon>
                <p className={cardCategory}>Capteur</p>
                <h3 className={cardTitle}>{cardData}</h3>
            </CardHeader>
            <CardFooter stats>
            <CustonDropdown />
            </CardFooter>
        </div>
    );
}

CardCapteur.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    cardData: PropTypes.string
};

export default withStyles(cardStyle)(CardCapteur);