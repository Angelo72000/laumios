import React, { Component } from 'react';
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Icon from "@material-ui/core/Icon";
import Button from "components/CustomButtons/Button.jsx";
import CustonDropdown from "components/CustonDropdown/CustonDropdown.jsx";
import classNames from "classnames";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import cardStyle from "assets/jss/material-dashboard-react/components/cardStyle.jsx";
import ColorPicker from "components/CustomInput/ColorPicker.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";


class CardLumio extends Component {
    state = {
        cardData: null,
        selectedAction: null,
        selectedColor: null,
        options: null,
        morseText: null,
        morseInput: null
    }

    executeOrder = () => {
        let workOrder = {
            lampNames: [],
            lampEffects: []
        };

        if (this.state.selectedAction !== null) {
            let action = this.state.selectedAction;

            workOrder.lampNames.push(this.state.cardData);
            workOrder.lampEffects.push(action.command);
            if (action.options === "rgb") {
                if (action.command === "off") {
                    workOrder.lampEffects.push("0");
                    workOrder.lampEffects.push("0");
                    workOrder.lampEffects.push("0");
                }
                else {
                    workOrder.lampEffects.push(this.state.selectedColor.rgb.r);
                    workOrder.lampEffects.push(this.state.selectedColor.rgb.g);
                    workOrder.lampEffects.push(this.state.selectedColor.rgb.b);
                }
            }
            else {
                workOrder.lampEffects.push("5");
                workOrder.lampEffects.push("5");
                workOrder.lampEffects.push("50");
            }
            if(action.command === "morse" && this.state.morseText !== null) {
                workOrder.lampEffects.push(this.state.morseText.replace(',', ''));
            }
            if(action.command === "progressive-fill") {
                workOrder.lampEffects.push(5000);
            }
        };

        console.log(workOrder);
        //fetch("http://10.110.6.111/lamp", {
            fetch("http://192.168.0.205/lamp", {
            method: "POST",
            body: JSON.stringify(workOrder)
        });
    }

    handleColorChange = (color) => {
        this.setState({ selectedColor : color});
    }

    handleTextChange = (text) => {
        console.log(text);
        this.setState({ morseText: text});
    }

    handleAction = (actionValue) => {
        let option;
        let inputMorse;
        if (actionValue.options === "rgb" && actionValue.command !== "off") {
            option = <ColorPicker onSelectColor={this.handleColorChange}/>;
        }
        else {
            option = null;
        }
        if(actionValue.command === "morse") {
            inputMorse = <CustomInput
            labelText="Morse..."
            id="morse-text"
            formControlProps={{
              fullWidth: true
            }}
            onTextChange={this.handleTextChange}
          />
        }
        else {
            inputMorse = null;
        }
        this.setState({ selectedAction: actionValue, options: option, morseInput: inputMorse });
    }

    render() {
        const actions = [
            {
                name: "Rain",
                command: "rain",
                options: "dfo"
            },
            {
                name: "Sun",
                command: "sun",
                options: "dfo"
            },
            {
                name: "Storm",
                command: "storm",
                options: "dfo"
            },
            {
                name: "Fill",
                command: "fill",
                options: "rgb"
            },
            {
                name: "ProgressiveFill",
                command: "progressive-fill",
                options: "rgb"
            },
            {
                name: "Morse",
                command: "morse",
                options: "rgb"
            },
            {
                name: "Off",
                command: "off",
                options: "rgb"
            }
        ];

        const {
            classes,
            className,
            cardCategory,
            cardTitle,
            cardData,
            ...rest
        } = this.props;

        this.state.cardData = cardData;

        const cardClasses = classNames({
            [classes.card]: true,
            [className]: className !== undefined
        });

        return (
            <div className={cardClasses} {...rest}>
                <CardHeader color="info" stats icon>
                    <CardIcon color="info" className='rotate'>
                        <Icon>wb_incandescent</Icon>
                    </CardIcon>
                    <p className={cardCategory}>Laumio</p>
                    <h3 className={cardTitle}>{cardData}</h3>
                </CardHeader>
                <CardBody>
                    <CustonDropdown listAction={actions} onSelectAction={this.handleAction} />
                    {this.state.options}
                    {this.state.morseInput}
                </CardBody>
                <CardFooter stats>
                    <Button color="success" onClick={this.executeOrder}>GO</Button>
                </CardFooter>
            </div>
        );
    }
}

CardLumio.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    cardData: PropTypes.string
};

export default withStyles(cardStyle)(CardLumio);