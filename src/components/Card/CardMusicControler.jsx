import React, { Component } from 'react';
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Icon from "@material-ui/core/Icon";
import RcSlider from "components/RcSlider/RcSlider.jsx";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";

import cardStyle from "assets/jss/material-dashboard-react/components/cardStyle.jsx";

class CardMusicControler extends React.Component {
    state = {
        isLoaded: false,
        value: 0
    }
    componentDidMount() {
        fetch("http://192.168.0.205/music/status")
            .then(res => {
                if (!res.ok) {
                    throw new Error("Fail to fetch datas");
                }
                return res.json();
            })
            .then(
                (result) => {
                    let playButton;
                    if (result === "play") {
                        playButton = <Button color="default" onClick={this.handlePause}><Icon>stop</Icon></Button>;
                    }
                    else {
                        playButton =  <Button color="default" onClick={this.handlePlay}><Icon>play_arrow</Icon></Button>;
                    }

                    this.setState({
                        isLoaded: true,
                        value: playButton
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        value: <Button color="default" onClick={this.handlePlay}><Icon>stop</Icon></Button>
                    });
                }
            );
    }

    handleClickNext = () => {

    }

    handleClickPrevious = () => {

    }

    handlePlay = () => {

    }

    handlePause = () => {

    }

    handleSetVolume = (volume) => {

    }

    render() {
        const {
            classes,
            className,
            cardCategory,
            cardTitle,
            cardData,
            ...rest
        } = this.props;
        const cardClasses = classNames({
            [classes.card]: true,
            [className]: className !== undefined
        });

        return (
            <div className={cardClasses} {...rest}>
                <CardHeader color="success" stats icon>
                    <CardIcon color="success">
                        <Icon>speaker</Icon>
                    </CardIcon>
                    <p className={cardCategory}>Capteur</p>
                    <h3 className={cardTitle}>{cardData}</h3>
                </CardHeader>
                <CardBody>
                    <RcSlider onSlideChanged={this.handleSetVolume} />
                </CardBody>
                <CardFooter>
                    <div>
                        <Button color="default" onClick={this.handleClickPrevious}><Icon>fast_rewind</Icon></Button>
                    </div>
                    <div>
                        {this.state.value}
                    </div>
                    <div>
                        <Button color="default" onClick={this.handleClickNext}><Icon>fast_forward</Icon></Button>
                    </div>
                </CardFooter>
            </div>
        );
    }
}



export default withStyles(cardStyle)(CardMusicControler);