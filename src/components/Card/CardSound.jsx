import React, { Component } from 'react';
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Icon from "@material-ui/core/Icon";
import RcSlider from "components/RcSlider/RcSlider.jsx";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";
import cardStyle from "assets/jss/material-dashboard-react/components/cardStyle.jsx";
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';



class CardSound extends React.Component {



    
  render() {

    const createSliderWithTooltip = Slider.createSliderWithTooltip;
    const Range = createSliderWithTooltip(Slider.Range);
    const Handle = Slider.Handle;
    
    const handle = (props) => {
      const { value, dragging, index, ...restProps } = props;
      return (
        <Tooltip
          prefixCls="rc-slider-tooltip"
          overlay={value}
          visible={dragging}
          placement="top"
          key={index}
        >
          <Handle value={value} {...restProps} />
        </Tooltip>
      );
    };
    
    
    
    function CardMusicControler({ ...props }) {
        const {
            classes,
            className,
            cardCategory,
            cardTitle,
            cardData,
            ...rest
        } = props;
        
        const cardClasses = classNames({
            [classes.card]: true,
            [className]: className !== undefined
        });
    
        const handle = (props) => {
            const { value, dragging, index, ...restProps } = props;
            return (
              <Tooltip
                prefixCls="rc-slider-tooltip"
                overlay={value}
                visible={dragging}
                placement="top"
                key={index}
              >
                <Handle value={value} {...restProps} />
              </Tooltip>
            );
          };
    
    
    
        return (
            <div className={cardClasses} {...rest}>
                <CardHeader color="success" stats icon>
                    <CardIcon color="success">
                        <Icon>speaker</Icon>
                    </CardIcon>
                    <p className={cardCategory}>Capteur</p>
                    <h3 className={cardTitle}>{cardData}</h3>
                </CardHeader>
                <CardBody>
                    <RcSlider />
                </CardBody>
                <CardFooter>
                    <div>
                        <Button color="default" onClick={''}><Icon>fast_rewind</Icon></Button>
                    </div>
                    <div>
                        <Button color="default" onClick={''}><Icon>stop</Icon></Button>
                    </div>
                        
    
                    <div>
                        <Button color="default" onClick={''}><Icon>play_arrow</Icon></Button>
                    </div>
    
                    <div>
                        <Button color="default" onClick={''}><Icon>fast_forward</Icon></Button>
                    </div>
                </CardFooter>
            </div>
        );
    }
    
    



        return (
            <CardMusicControler />
        );
      };
      

  }


export default CardSound;
