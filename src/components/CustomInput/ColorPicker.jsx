import React from 'react';
import { SketchPicker, ChromePicker } from 'react-color';

class ColorPicker extends React.Component {
    state = {
        background: '#fff',
    };

    handleChangeComplete = (color) => {
        this.setState({ background: color.hex });
        this.props.onSelectColor(color);
        this.setState({ displayColorPicker: false })
    };

    render() {
        return <ChromePicker color={ this.state.background }  onClose={this._handleClose} onChangeComplete={this.handleChangeComplete} />;
    }
}

export default ColorPicker;