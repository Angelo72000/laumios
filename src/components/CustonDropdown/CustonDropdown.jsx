import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class SimpleMenu extends React.Component {
  pickAnAction = "Pick an action";
  state = {
    selectedAction: "Pick an action",
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = (element) => {
    this.setState({ 
      anchorEl: null,
      selectedAction: element.name !== undefined ? element.name : element
    });

    if(element !== this.pickAnAction) {
      this.props.onSelectAction(element);
    }
  };

  render() {
    const { anchorEl } = this.state;

    let actionsMenu = this.props.listAction;
    var menuActions = [];
    if(actionsMenu !== undefined) {
      actionsMenu.forEach(element => {
        menuActions.push(<MenuItem onClick={this.handleClose.bind(this, element)}>{element.name}</MenuItem>);
      });
    }

    return (
      <div> 
        <Button
          aria-owns={anchorEl ? 'simple-menu' : undefined}
          color="transparent"  
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          {this.state.selectedAction}
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose.bind(this, this.pickAnAction)}
        >
          {menuActions}
        </Menu>
      </div>
    );
  }
}

export default SimpleMenu;
