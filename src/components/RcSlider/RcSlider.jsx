import React from 'react';
import 'rc-slider/assets/index.css';
import Slider, { Range } from 'rc-slider';



class RcSlider extends React.Component {
  volumeChanged = (newVolume) => {
    console.log(newVolume);
  }
  render() {
    return (
      <div style={{ "width": "100%" }} >
          <Slider min={0} max={200} onAfterChange={this.volumeChanged}/>    
        {/* <Slider min={0} max={20} /> */}
      </div>
    );
  }
}

export default RcSlider;
