import LocationOn from "@material-ui/icons/LocationOn";
import Laumios from "../views/Dashboard/Laumios";

const dashboardRoutes = [

  {
    path: "/Laumios",
    sidebarName: "Laumios",
    navbarName: "Laumios",
    icon: LocationOn,
    component: Laumios
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
