import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CardLumio from "components/Card/CardLumio.jsx";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import CardMusicControler from "components/Card/CardMusicControler.jsx";

class Laumios extends React.Component {
    state = {
        value: 0,
        isLoaded: false
    };
    handleChange = (event, value) => {
        this.setState({ value });
    };

    handleChangeIndex = index => {
        this.setState({ value: index });
    };

    componentDidMount() {
        // this.setState({
        //     isLoaded: true,
        //     value: ["DummyLocal1", "DummyLocal2", "DummyLocal3", "DummyLocal4", "DummyLocal5", "DummyLocal6"]
        // });
        
        //fetch("http://10.110.6.111/discovery")
        fetch("http://192.168.0.205/discovery")
            .then(res => {
                if(!res.ok) {
                    throw new Error("Fail to fetch datas");
                }
                return res.json();
            })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        value: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        value: ["DummyLocal1", "DummyLocal2", "DummyLocal3", "DummyLocal4", "DummyLocal5", "DummyLocal6"]
                    });
                }
            );
    }


    render() {
        function ListItem(props) {
            var result = [];
            for (let index = 0; index < props.value.length; index++) {
                const laumio = props.value[index];
                result.push(<GridItem xs={12} sm={6} md={4}>
                    <CardLumio cardData={laumio} cardCategory={props.valueCardCategory} cardTitle={props.valueCardTitle}>
                    </CardLumio>
                </GridItem>);
            }

            return result;
        }


        const { classes } = this.props;

        return (
            <div>
                <GridContainer>
                    <ListItem value={this.state.value} valueCardCategory={classes.cardCategory} valueCardTitle={classes.cardTitle} />
                </GridContainer>
                <GridContainer>
                    <GridItem xs={12} sm={6} md={6}>
                        <CardMusicControler />
                    </GridItem>
                </GridContainer>
            </div>
        );
    }
}

Laumios.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Laumios);
